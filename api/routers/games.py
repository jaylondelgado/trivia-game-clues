from datetime import datetime
from fastapi import APIRouter, Response, status
from pydantic import BaseModel
import psycopg
from fastapi.responses import JSONResponse

from .clues import Clue

router = APIRouter()

class Game(BaseModel):
    id: int
    episode_id: int
    aired: str
    canon: bool
    total_won: int
    
class CustomGame(BaseModel):
    id: int
    created_on: datetime
    clues: list[Clue]

class Message(BaseModel):
    message: str

@router.get(
    "/api/games/{game_id}",
    response_model=Game,
    responses={404: {"model": Message}},
)
def get_game(game_id: int, response: Response):
    with psycopg.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(
                f"""
                SELECT games.id, games.episode_id, games.aired, games.canon, sum(clues.value) AS total_won
                FROM games
                INNER JOIN clues ON (clues.game_id=games.id)
                WHERE clues.game_id = %s
                GROUP BY games.id
                """,
                [game_id]
            )
            row = cur.fetchone()
            if row is None:
                return JSONResponse(status_code=404, content={"Game": "Game not found"})
            record = {}
            for i, column in enumerate(cur.description):
                record[column.name] = row[i]
            return record

@router.post(
  "/api/custom-games",
  response_model=CustomGame
)
def create_custom_game():
  with psycopg.connect() as conn:
    with conn.cursor() as cur:
      cur.execute(
        """
        SELECT cats.id, cats.title, cats.canon,
          clues.id, clues.question, clues.answer,
          clues.value, clues.invalid_count,
          clues.canon, clues.category_id
        FROM categories AS cats
        INNER JOIN clues ON (cats.id = clues.category_id)
        WHERE clues.invalid_count = 0 AND cats.canon = true
        ORDER BY RANDOM()
        LIMIT 30;
        """
      )
      clues = cur.fetchall()
      with conn.transaction():
        cur.execute(
          """
          INSERT INTO game_definitions (created_on)
          VALUES (CURRENT_TIMESTAMP);
          """
        )
        cur.execute(
          """
          SELECT gd.id, gd.created_on
          FROM game_definitions gd
          ORDER BY gd.created_on DESC
          LIMIT 1;
          """
        )
        new_game_definition = cur.fetchone()
        new_game_definition_id = new_game_definition[0]

        for clue in clues:
          current_clue_id = clue[0]
          cur.execute(
            """
            INSERT INTO game_definition_clues (game_definition_id, clue_id)
            VALUES (%s, %s)
            """,
            [new_game_definition_id, current_clue_id],
          )
        return {
          "id": new_game_definition_id,
          "created_on": new_game_definition[1],
          "clues":
          [{
             "id": clue[3],
                "question": clue[4],
                "answer": clue[5],
                "value": clue[6],
                "invalid_count": clue[7],
                "canon": clue[8],
                "category": {
                  "id": clue[0],
                  "title": clue[1],
                  "canon": clue[2],
                }
              } for clue in clues]
        }

